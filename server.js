let express = require('express'),
    bodyParser = require('body-parser'),
    cors = require('cors'),
    morgan = require('morgan'),
    app = express();
const multer = require('multer');
//  list routes service
let routesIndex = require('./app/routes/routesHome');

app.use(cors());
// app.use(forms.array()); 
app.use(
    bodyParser.urlencoded({
        extended: true
    })
);
app.use(cors());
app.use(
    bodyParser.urlencoded({
        extended: true
    })
);


app.use(morgan('dev'));
app.use(bodyParser.json());
app.use(routesIndex);
app.use(express.static(__dirname));


module.exports = app;
