let express = require('express');
let router = express.Router();

// service
const serviceProcess = require('../services/servicesProcess');

router.post('/combination', serviceProcess.combinationMaxMin);
router.post('/count_number', serviceProcess.countNumber);


module.exports = router;