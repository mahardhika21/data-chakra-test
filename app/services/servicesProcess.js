async function combinationMaxMin(req, res) {
    try {
        console.log(req.body);
        const input = req.body;
        let max = 0;
        let min = 99999;
        let total = 0;
        for (let i of input) {
            if (i > max) {
                max = i;
            }
            if (i < min) {
                min = i;
            }
            total = total + i;
        }

         return res.json({
            success: true,
            messgae: 'success',
            data: {
                max: total - min,
                min : total - max,
            }
        });
    } catch (err) {
        return res.json({
            success: false,
            messgae: 'falied',
            error: err.stack
        });
    }
}

async function countNumber(req, res) {
    try {
        console.log(req.body);
        const n = req.body.n;
        const arr = req.body.arr;
        
         return res.json({
            success: true,
            messgae: 'success',
            data: await calculate(n, arr)
        });
    } catch (err) {
        return res.json({
            success: false,
            messgae: 'falied',
            error: err.stack
        });
    }
}

async function calculate(n, arr)
    {
        let tempArr = new Array(n+1);
        tempArr.fill(0);
        tempArr[0] = 1;
        for (let i = 1; i <= n; i++) {
            for (let j = 0; j < arr.length; j++) {
                if (i >= arr[j]) {
                    tempArr[i] += tempArr[i - arr[j]];
                }
            }
        }
        return tempArr[n];  
    }
     

module.exports = {
    combinationMaxMin, countNumber
}